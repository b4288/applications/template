variable "private_key_path" {
  description = "Path to the private SSH key, used to access the instance."
  default     = "~/.ssh/id_rsa"
}

variable "pdns_api_key" {
  description = "API KEY for Powerdns"
  default = "xxxxxxXXXXXxxxxx"
}

