terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
    }
    powerdns = {
      source = "pan-net/powerdns"
    }
  }
}

// instance the provider
provider "libvirt" {
  // uri = "qemu:///system"
  uri = "qemu+ssh://admuser@devbox3/system"
}

provider "powerdns" {
  api_key    = "${var.pdns_api_key}"
  server_url = "https://pdns.robert.local"
}

// variables that can be overriden
variable "hostname" { default = "test" }
variable "domain" { default = "robert.local" }
variable "ip_type" { default = "dhcp" } # dhcp is other valid type
variable "memoryMB" { default = 1024 * 1 }
variable "cpu" { default = 1 }

// fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "base" {
  name   = "ubuntu-base"
  source = "https://cloud-images.ubuntu.com/minimal/daily/impish/20220308/impish-minimal-cloudimg-amd64.img"
  pool   = "devbox"
  format = "qcow2"
}

resource "libvirt_volume" "test-os_image" {
  name            = "test-os_image"
  base_volume_id  = libvirt_volume.base.id
  pool            = "devbox"
  size            = 10737418240
}

// Use CloudInit ISO to add ssh-key to the instance
resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.hostname}-commoninit.iso"
  pool           = "devbox"
  user_data      = data.template_cloudinit_config.config.rendered
  network_config = data.template_file.network_config.rendered
}

data "template_file" "user_data" {
  template = file("${path.module}/cloud_init.cfg")
  vars = {
    hostname   = var.hostname
    fqdn       = "${var.hostname}.${var.domain}"
    public_key = file("~/.ssh/id_rsa.pub")
  }
}

data "template_cloudinit_config" "config" {
  gzip          = false
  base64_encode = false
  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.user_data.rendered
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config_${var.ip_type}.cfg")
}

// Create the machine
resource "libvirt_domain" "domain-ubuntu" {
  # domain name in libvirt, not hostname
  name       = var.hostname
  memory     = var.memoryMB
  vcpu       = var.cpu
  autostart  = true
  qemu_agent = true
  timeouts {
    create = "20m"
  }


  disk {
    volume_id = libvirt_volume.test-os_image.id
  }
  network_interface {
    network_name = "bridged-network"
    mac          = "52:54:00:36:14:e5"
    wait_for_lease = true
  }

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = "true"
  }

  provisioner "remote-exec" {
    inline = [
      "cloud-init status --wait",
    ]

    connection {
      host     = "${self.network_interface.0.addresses.0}"
      type     = "ssh"
      user     = "admuser"
      private_key = "${file("~/.ssh/id_rsa")}"
    }
  }

  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u admuser -i ${self.network_interface.0.addresses.0}, --private-key ${var.private_key_path} provision.yml"
  }
}

terraform {
  required_version = ">= 0.12"
}

output "ips" {
  value = libvirt_domain.domain-ubuntu.network_interface.0.addresses.0
}

resource "powerdns_record" "test" {
  zone    = "robert.local."
  name    = "test.robert.local."
  type    = "A"
  ttl     = 300
  records = [libvirt_domain.domain-ubuntu.network_interface.0.addresses.0]
}



